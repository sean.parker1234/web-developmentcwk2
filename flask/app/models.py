import hashlib
from app import db, login
from datetime import datetime
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash

#this table stores who are friends with who
friends = db.Table('friends',
    db.Column('follower_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('followed_id', db.Integer, db.ForeignKey('user.id'))
    )

#user class used when a person signs up to the blog, validation done via form so not needed here
class User(UserMixin, db.Model):
    firstName = db.Column(db.String(50))
    lastName = db.Column(db.String(75))
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), index=True, unique=True)
    email = db.Column(db.String(110),index=True, unique=True)
    passwordHash = db.Column(db.String(128))
    followersCount = db.Column(db.Integer)
    personalProfile = db.Column(db.String(150))
    posts = db.relationship('Post',backref='author', lazy='dynamic')
    #This describes the relationship of friends and links user to user
    friendsRelationship = db.relationship( 'User', secondary=friends,
    #this shows how the left coloumn of the friends table is linked to User
    primaryjoin = (friends.c.follower_id == id),
    #this shows how the right coloumn of the friends table is linked to User
    secondaryjoin = (friends.c.followed_id == id),
    #dynamic means the query will not run until the user requests to follow someone
    backref = db.backref('friends', lazy='dynamic'), lazy='dynamic' )

    #format functions tells python how to print objects, good for debugging
    def __repr__(self):
        return '<User {}>'.format(self.username)

    def userPicture(self,size):
        #creating a user profile image using gravatar
        #uses hashed email to give user a unqiue profile picture
        email= self.email
        pic_type="retro"
        email_hash =hashlib.md5(email.lower().encode('utf-8')).hexdigest()
        gravatar_url = "https://www.gravatar.com/avatar/{}?d={}&s={}".format(email_hash, pic_type,size)

        return gravatar_url


    #functions which create and check passswords match
    def pass_create(self, rawPassword):
        self.passwordHash = generate_password_hash(rawPassword)

    def pass_check(self, rawPassword):
        return check_password_hash(self.passwordHash, rawPassword)

    #the model needs to check if the user is following another user already
    def friends_already(self, user):
        #uses chcks table on left side for self.id returns true s already follows
        return self.friendsRelationship.filter(friends.c.followed_id == user.id).count() > 0

    def addFriend(self, user):
        if self.friends_already(user) == False:
            self.friendsRelationship.append(user)
            self.followersCount = user.followersCount+1

    def removeFriend(self,user):
        if self.friends_already(user) == True:
            self.friendsRelationship.remove(user)
            self.followersCount = user.followersCount-1

    def friendsPosts(self):
        #query uses a join to join friends to post table by followed_id to user_id of that post
        #then filters results to only show ones where follower_id == user_id
        #which gives a list of all posts by friends of current_user
        return Post.query.join(friends,(friends.c.followed_id == Post.user_id)).filter(friends.c.follower_id == self.id)

        #this function queries database for all user posts
    def userPosts(self):
        return Post.query.filter_by(user_id = self.id)
        #joins user posts and friends posts and then orders them in descending order
    def joinPosts(self):
        friends = self.friendsPosts()
        user = self.userPosts()
        return friends.union(user).order_by(Post.timestamp.desc())

class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200))
    body = db.Column(db.String(500))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    likes = db.Column(db.Integer)
    


#As user navigates pages it will need to keep track of user id by storing in user session
#this function loads the user given their id from that session
@login.user_loader
def user_load(id):
    return User.query.get(int(id))
