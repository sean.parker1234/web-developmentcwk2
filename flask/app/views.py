from flask import render_template, flash, redirect, url_for, request, session
from app import app
from .forms import SignUpForm, LoginForm, PostForm, SettingForm, searchForm
from app import db,models
from flask_login import current_user, login_user
from flask_login import logout_user, login_required
import json


@app.route('/')
def index():
    return render_template("index.html")

@app.route('/login', methods=['GET','POST'])
def login():
    #stops logged in user navigating to /login url
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        #queries database for username
        user = models.User.query.filter_by(username=form.username.data).first()
        #this check is no user name was found or password is incorrect and redirects to login if thats the case
        if user is None or not user.pass_check(form.rawPassword.data):
            flash("Invalid username or password")
            return render_template("login.html",title="Login", form=form)
        #login_user() sets current_user to that who just logged in so
        #user can be kept track of over several pages
        login_user(user)
        session['logged_on'] = True
        return render_template("index.html")##change to homepage later
    return render_template("login.html", title="Login", form=form)


@app.route('/signup', methods=['GET','POST'])
def signup():
    form = SignUpForm()
    #checks th form is valid before submitting
    if form.validate_on_submit():

        #makes a user obj and passes it all data from form
        user = models.User(firstName=form.first_name.data, lastName=form.last_name.data,username=form.username.data,email=form.email.data, followersCount=0)
        #creates the password hash from raw password
        user.pass_create(form.rawPassword.data)

        #commits user to data base
        db.session.add(user)
        db.session.commit()

        flash('sign up succesfull')
        return render_template("index.html")
    return render_template("signup.html", form=form)

@app.route('/logout')
def logout():
    logout_user()
    session['logged_on'] = False
    return render_template("index.html")

@app.route('/user/<username>', methods=['GET','POST'])
#this checks the user is logged in before they can access any homepage
@login_required
def homePage(username):
    #obtains the user information by getting username from current user and querying
    #database to get user obj
    user = models.User.query.filter_by(username=username).first()

    #this section chcks if user == currentuser to show their post + followrs posts
    #otherwise it will just users posts
    if current_user == user:
        posts = current_user.joinPosts().all()

    else:
        posts = user.userPosts().all()
    form = PostForm()
    #checks the frorm is valid an submits it to database
    if form.validate_on_submit():
        post = models.Post(title=form.title.data, body=form.postArea.data, user_id=current_user.id, likes=0)
        flash("Your post has been published")
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('homePage', username=username))
    return render_template("homePage.html",user=user, form=form,posts=posts)

@app.route('/addFriends/<username>')
@login_required
def addFriends(username):
    user = models.User.query.filter_by(username=username).first()
    current_user.addFriend(user)
    db.session.commit()
    flash("Congrats! you and {} are now buddies for life".format(username))
    return redirect(url_for('homePage', username=username))

@app.route('/removeFriends/<username>')
@login_required
def removeFriends(username):
    user = models.User.query.filter_by(username=username).first()
    current_user.removeFriend(user)
    db.session.commit()
    flash("Sorry to hear you and {} are no longer".format(username))
    return redirect(url_for('homePage', username=username))


@app.route('/settings', methods=['GET','POST'])
@login_required
def settings():
    user=current_user
    form = SettingForm()
    #print(form)
    if form.validate_on_submit():
        ##passes data to current user and commits to database
        current_user.personalProfile = form.personalProfile.data
        #check if user has tried to change password
        if form.rawPassword.data is not None and len(form.rawPassword.data) > 0:
            current_user.pass_create(form.rawPassword.data)
        db.session.commit()
        flash("Update Successfull")
        return redirect(url_for('homePage',username=current_user.username))
    #needs to prepopulate personalProfile field so need to check if the page is getting or posting
    elif request.method == 'GET':
        form.personalProfile.data = current_user.personalProfile
        flash("Form loaded")
    else:
        flash("fail")
    return render_template('settings.html',title="Settings",form=form, user=user)

@app.route('/search', methods=['GET','POST'])
@login_required
def search():
    #gets all users to display on page
    users= models.User.query.all()
    form = searchForm()
    #if user fills out search bar in correct mannor then this section runs
    if form.validate_on_submit():
        #tries to find user of that username entered
        user = models.User.query.filter_by(username=form.search.data).first()
        #if not none then user has been found and user is directed to homePage of searched username
        if user is not None:
            return redirect(url_for('homePage', username=user.username))
        else:
            flash("Sorry that username was not found, please try again")
    return render_template('search.html',title="search",form=form,users=users)

@app.route('/like',methods=['POST'])
def like():
    #loads json data and gets post from it
    data = json.loads(request.data)
    post_id = int(data.get('response'))
    post = models.Post.query.get(post_id)



    return json.dumps({'status':'OK', 'response': response})
