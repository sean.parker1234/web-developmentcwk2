from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from celery import Celery
from flask_login import LoginManager
from logging import FileHandler, WARNING

app = Flask(__name__)

file_handler = FileHandler('errorLog.txt')
file_handler.setLevel(WARNING)

app.logger.addHandler(file_handler)
app.config.from_object('config')

#init database
db = SQLAlchemy(app)

#init bootstrap
boostrap = Bootstrap(app)

#init migrations
migrate = Migrate(app, db)

#init Login manager
login = LoginManager(app)
from app import views, models
