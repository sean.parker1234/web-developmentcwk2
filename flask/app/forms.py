from flask_wtf import Form
##remember to import any filds or validators
from app import models,db
from wtforms import IntegerField, StringField, PasswordField
from wtforms.validators import DataRequired, Length, Email, EqualTo
from wtforms.widgets import TextArea

class SignUpForm(Form):
    first_name = StringField('First Name',validators=[DataRequired(),Length(min=1, max=50)])
    last_name = StringField('Last Name',validators=[DataRequired(),Length(min=1, max=75)])
    username = StringField('username', validators=[DataRequired(), Length(min=6, max=50)])
    email = StringField(validators=[Email()])
    rawPassword = PasswordField(validators=[DataRequired(), Length(min=6, max=50)])
    confirmPassword = PasswordField('Confirm Password', validators=[DataRequired(),EqualTo('rawPassword')])

    #as email and user name are unqiue then they need to be validated in the databse
    #to see if they have already been used, flask automatically adds these as validators
    def validate_username(self,username):
        #queries database and filters by username, will just return first match
        user=models.User.query.filter_by(username=username.data).first()
        #.first() retuns user obj if query finds one or None if not
        if user is not None:
            self.username.errors.append("Username is already being used, please choose another")
            return False

    def validate_email(self,email):
        email =models.User.query.filter_by(email=email.data).first()
        #.first() retuns user obj if query finds one or None if not
        if email is not None:
            self.email.errors.append("Email is already being used, please choose another")
            return False

class LoginForm(Form):
    username = StringField('Username', validators=[DataRequired(),Length(min=6, max=50)])
    rawPassword = PasswordField(validators=[DataRequired(),Length(min=6, max=50)])

class PostForm(Form):
    title=StringField("Title", validators=[DataRequired(),Length(min=1,max=200)])
    postArea = StringField("post", widget=TextArea())

class SettingForm(Form):
    personalProfile = StringField(widget=TextArea())
    rawPassword = PasswordField()
    confirmPassword = PasswordField(validators=[EqualTo('rawPassword')])

    def validate_rawPassword(self,rawPassword):
        if rawPassword.data =='' or self.confirmPassword.data == '':
            return True
        if len(rawPassword.data)<6 or len(self.confirmPassword.data)<6:
            self.rawPassword.errors.append("Passwords need to be 6 characters or more")
            return False
        else:
            if rawPassword.data == self.confirmPassword.data:
                return True

class searchForm(Form):
    search = StringField('Search By Username',validators=[DataRequired(),Length(min=6, max=50)])
