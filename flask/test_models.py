import unittest
from app import app, db
from datetime import datetime
from werkzeug.security import generate_password_hash, check_password_hash
from app.models import User, Post


class UserModelCase(unittest.TestCase):
    #create temp database that can be manipulated
    def setUp(self):
        app.config['SQLALCHEMY_DATABASE_URI']='sqlite://'
        db.create_all()

    def tearDown(self):
        ##remove database afterwards
        db.session.remove()
        db.drop_all()

    def test_userProfile(self):
        #checks address returned match to pre made one
        testUser = User(firstName='John', lastName='Doe', username='johnDoe123', email='john@testmail.com')
        self.assertEqual(testUser.userPicture(80), ('https://www.gravatar.com/avatar/c1512f029c24c9d1c59a1c6e75f0520f?d=retro&s=80'))

    def test_check_hash_pass(self):
        #check match passwords pass
        u = User(username='testuser',email='testemial@email.com')
        u.pass_create('password')
        self.assertTrue(u.pass_check('password'),True)

    def test_check_hash_fail(self):
        ##checks unmatched passwords fail
        u = User(username='testuser',email='testemial@email.com')
        u.pass_create('password')
        self.assertFalse(u.pass_check('pass'),False)

    def test_adding_friend(self):
        testUser1 = User(firstName="John", lastName="Doe", username="johnDoe123", email="john@testmail.com",followersCount=0, personalProfile="None")
        testUser2 = User(firstName="Chloe", lastName="Alison", username="chloeAlison123", email="chloe@testmail.com",followersCount=0, personalProfile="None")
        testUser1.pass_create("pass")
        testUser2.pass_create("pass")

        #checks there are no existin relationships
        db.session.add(testUser1)
        db.session.add(testUser2)
        db.session.commit()
        self.assertEqual(testUser1.friends.all(),[])
        self.assertEqual(testUser1.friendsRelationship.all(),[])

        testUser1.addFriend(testUser2)

        db.session.commit()
        #checks friends already works
        self.assertTrue(testUser1.friends_already(testUser2))
        self.assertFalse(testUser2.friends_already(testUser1))
        #check relationship exisits
        self.assertEqual(testUser1.friendsRelationship.count(),1)

    def test_removing_friend(self):
        testUser1 = User(firstName="John", lastName="Doe", username="johnDoe123", email="john@testmail.com",followersCount=0, personalProfile="None")
        testUser2 = User(firstName="Chloe", lastName="Alison", username="chloeAlison123", email="chloe@testmail.com",followersCount=0, personalProfile="None")
        testUser1.pass_create("pass")
        testUser2.pass_create("pass")


        db.session.add(testUser1)
        db.session.add(testUser2)
        testUser1.addFriend(testUser2)
        db.session.commit()
        #removes tesUser2 as friend
        testUser1.removeFriend(testUser2)
        #asserts that both users are not friends
        self.assertFalse(testUser1.friends_already(testUser2))
        self.assertFalse(testUser2.friends_already(testUser1))
        #checks there is not connections in the association table
        self.assertEqual(testUser1.friendsRelationship.count(),0)

    def test_posts(self):
        testUser1 = User(firstName="John", lastName="Doe", username="johnDoe123", email="john@testmail.com",followersCount=0, personalProfile="None")
        testUser2 = User(firstName="Chloe", lastName="Alison", username="chloeAlison123", email="chloe@testmail.com",followersCount=0, personalProfile="None")
        testUser3 = User(firstName="monty", lastName="Python", username="JohnCleese", email="Cleese@mail.com", followersCount=0, personalProfile="None")
        testUser1.pass_create("pass")
        testUser2.pass_create("pass")
        testUser3.pass_create("pass")

        db.session.add(testUser1)
        db.session.add(testUser2)
        db.session.add(testUser3)
        testUser1.addFriend(testUser2)
        p1 =Post(title='user1 post',body='testUser1 post for test', timestamp=datetime.utcnow(), author=testUser1)
        p2 = Post(title='user2 post', body='testUser2 post for test', timestamp=datetime.utcnow(),author=testUser2)
        p3 = Post(title='user3 post', body='testUser3 post for test', timestamp=datetime.utcnow(),author=testUser3)
        db.session.add(p1)
        db.session.add(p2)
        db.session.add(p3)
        db.session.commit()

        testUser2.addFriend(testUser1)
        db.session.commit()
        #checks that the friends post that show are just the friends
        self.assertEqual(testUser2.friendsPosts().all(), [p1])
        self.assertEqual(testUser2.userPosts().all(), [p2])
        self.assertEqual(testUser2.joinPosts().all(), [p2,p1])






if __name__ == '__main__':
    unittest.main()
